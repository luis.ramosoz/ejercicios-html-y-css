var personArr = [
   {
      "personId": 123,
      "name": "Jhon",
      "city": "Melbourne",
      "phoneNo": "1234567890"
   },
   {
      "personId": 124,
      "name": "Amelia",
      "city": "Sydney",
      "phoneNo": "1234567890"
   },
   {
      "personId": 125,
      "name": "Emily",
      "city": "Perth",
      "phoneNo": "1234567890"
   },
   {
      "personId": 126,
      "name": "Abraham",
      "city": "Perth",
      "phoneNo": "1234567890"
   }
];

var tbody = document.getElementsByClassName('tBody');

for (var i = 0; i < personArr.length; i++) {
   var tr = "<tr>";

   if (personArr[i].value.toString().substring(personArr[i].value.toString().indexOf('.'), personArr[i].value.toString().length)) {
      personArr[i].value += "0";
   }
}